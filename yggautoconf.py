#!/usr/bin/env python3

# Copyright 2020, sapient_cogbag <sapient_cogbag@protonmail.com>
#
# Copying and distribution of this file, with or without modification, 
# are permitted in any medium without royalty, provided the copyright notice 
# and this notice are preserved. This file is offered as-is, without any 
# warranty.

import shutil
import shlex
import os
import os.path
import sys
import math
import argparse
import tempfile
import subprocess
import bisect
import abc
import itertools
import string
import random
import time
import collections

import typing
from typing import *

import json
import subprocess
import contextlib

import enum


T = TypeVar('T')
V = TypeVar('V')

# For compatibility, Protocol was only added in python 3.8 >.<
if "Protocol" not in vars():
    global Protocol

    # Just adds a simple generic.
    class _Protocol(typing.Generic[T]):
        pass
    Protocol=_Protocol





#########################################################################################
####################### ARGPARSE UTILITIES ##############################################
#########################################################################################



def enum_argparse(enum_cls: enum.Enum, case_sensitive:bool=False, set_repr:bool=True) -> enum.Enum:
    """
    Handle to make enums work well with argparse.

    This overrides the __str__ method to return the name of the enum member name,
    if case insensitive, the lowercased enum name.

    We also allow to set __repr__ method, which gives nicer error messages when 
    invalid values are provided.

    It sets two items on the enum class (as class methods) too. .ap_type, to be used as the type= parameter,
    and .ap_choices, to be used as the choices= parameter.
    """
    def strconv(self):
        v = self.name

        if not case_sensitive:
            v = v.lower()
        return v

    def reprconv(self):
        return str(self)

    enum_cls.__str__ = strconv

    if set_repr:
        enum_cls.__repr__ = reprconv

    def con_lower(v: str):
        """Conditional string lowercasing"""
        return v if case_sensitive else v.lower()

    def gen_namedict(cls: Type) -> Dict[str, Any]:
        return {con_lower(k): ev for k, ev in cls.__members__.items()}

    @classmethod
    def ap_type(cls: Type, string_value: str):
        # Map of conditionally-lowercased enum value names to enum values.
        names: Dict[str, Any] = gen_namedict(cls)
        if not con_lower(string_value) in names.keys():
            raise argparse.ArgumentTypeError("Invalid choice")

        return names[con_lower(string_value)]
       
    enum_cls.ap_choices = [v for v in gen_namedict(enum_cls).values()]
    enum_cls.ap_type = ap_type

    return enum_cls


def criterion_type(t: Type, criterion: Callable[[Any], bool], errormsg="{s!v} did not match criterion") -> Callable[[str], Any]:
    """
    Creates a type= (for argparse) parameter which applies the given criterion to test
    the original type.

    The result, if the criterion is not matched, is passed as `v` in a .format call on
    errormsg
    """
    def _res(stringv):
        res = t(stringv)
        if not criterion(res):
            raise argparse.ArgumentTypeError(errormsg.format(v=res))
        return res
    return _res


def port(string):
    """
    Type for network ports to be used with argparse.
    """
    # I am not 100% sure if 65535 is valid since all-1s in binary probably has
    # special meaning for ports ^.^
    return criterion_type(int, lambda x: x > 0 and x < 65535, "{s!v} is not a valid port")(string)












###################################################################################
################# GENERAL DEFINITIONS #############################################
###################################################################################



# These are used to detect generic peer strings of the given specified type, 
# and replace the location and port of the actual running tor/i2p node within the string.
DEFAULT_TOR_PORTS = [9050, "<tor-port>"]
DEFAULT_I2P_PORTS = [4447, "<i2p-port>"]

DEFAULT_TOR_NODE_HOSTS = ["localhost", "127.0.0.1", "<tor-host>"]
DEFAULT_12P_NODE_HOSTS = ["localhost", "127.0.0.1", "<i2p-host>"]


FilePath = NewType("FilePath", str)
Region = NewType("Region", str)


SECONDS_PER_DAY = 24 * 3600 # Of course only rough but this is just for estimates.



# List of (various region paths, depths, arbitrary data)
OrderedRegionDataCollection = List[Tuple[Region, int, T]]
OrderedRegionCollection = OrderedRegionDataCollection[List[FilePath]]



@enum_argparse
@enum.unique
class PROGRAM_MODE(enum.Enum):
    """
    Sets what mode the program runs in.

    In effect, this is a selector for different things the script can do.
    """
    MODIFY_CONFIG = enum.auto(),
    LIST_REGIONS = enum.auto(),
    LIST_SELECTED_PEERSET = enum.auto()
    LIST_SELECTED_PEERSET_NO_LOCALISE = enum.auto()


def by_default(v: Optional[T], default: T) -> T:
    """
    Get a value by default.
    """
    return v if v is not None else default


class PriviligedContext(contextlib.AbstractContextManager):
    """
    Holds a privileged context for the given program mode.
    """
    def __init__(self, mode: PROGRAM_MODE, tmp_directory: str, sudoids: Tuple[Optional[int], Optional[int]], parsed_args: argparse.Namespace):
        """
        Does what you think.

        mode is the program mode we are running in
        can_drop_privs is true if we are in a sudo context and also root.
        tmp_directory is the location of the overall program (rather than --work subprocesses)
        sudoids is a pair of sudo_uid and sudo_gid that represent what we will be dropping privs
        to if we can.
        parsed_args is the arguments to the main program, parsed.

        After the functions in this are run, the directory will get chmodded to 0o755
        and chowned to the sudo ids if we can do priv deescalation.

        We switch back to root and chown root our temp directory before exit stuff is 
        called.
        """
        self._mode = mode
        self._tmp_directory = tmp_directory
        self._sudo_uid = sudoids[0] if sudoids is not None else None
        self._sudo_gid = sudoids[1] if sudoids is not None else None
        self._can_drop_privs = (
            os.geteuid() == 0 
            and self._sudo_uid is not None 
            and self._sudo_gid is not None
        )
        self._parsed_args = parsed_args

    def _enter_list_regions(self):
        """
        Enter for listing regions.
        """
        pass

    def _enter_peerlisting_modes(self):
        pass

    def _exit_list_regions(self):
        pass

    def _exit_peerlisting_modes(self):
        pass

    def _enter_modify_config(self):
        """
        Set up the config file.
        """
        # This is our temporary configurator :)
        self._tmpconfig = os.path.join(self._tmp_directory, "tmp_yggdrasil_config.conf")
        # If the file does not exist, create it...
        if not os.path.exists(self._parsed_args.config_file):
            with open(self._parsed_args.config_file, "wt") as fd:
                fd.write("{}")
                fd.close()
        # Get the old file to that spot ^.^
        shutil.copy(self._parsed_args.config_file, self._tmpconfig)
        # Change the config file. 
        if self._can_drop_privs:
            os.chmod(self._tmpconfig, 0o644)
            os.chown(self._tmpconfig, self._sudo_uid, self._sudo_gid)

    def _preenter(self):
        {
            PROGRAM_MODE.LIST_REGIONS: self._enter_list_regions,
            PROGRAM_MODE.LIST_SELECTED_PEERSET: self._enter_peerlisting_modes,
            PROGRAM_MODE.LIST_SELECTED_PEERSET_NO_LOCALISE: self._enter_peerlisting_modes,
            PROGRAM_MODE.MODIFY_CONFIG: self._enter_modify_config
        } [self._mode]()

    def _exit_modify_config(self):
        """
        Beforehand, copy...
        """
        if self._can_drop_privs:
            os.chown(self._tmpconfig, 0, 0)

        shutil.copy(self._tmpconfig, self._parsed_args.config_file)

    def _postexit(self):
        {
            PROGRAM_MODE.LIST_REGIONS: self._exit_list_regions,
            PROGRAM_MODE.LIST_SELECTED_PEERSET: self._exit_peerlisting_modes,
            PROGRAM_MODE.LIST_SELECTED_PEERSET_NO_LOCALISE: self._exit_peerlisting_modes,
            PROGRAM_MODE.MODIFY_CONFIG: self._exit_modify_config
        } [self._mode]()

    def __enter__(self):
        self._preenter()
        if self._can_drop_privs:
            # make it so we can at least read it in the future
            # and see stuff exists.
            os.chmod(self._tmp_directory, 0o755)  # dirs need execute perms
            os.chown(self._tmp_directory, self._sudo_uid, self._sudo_gid)

        return self

    
    def __exit__(self, *exc):
        if self._can_drop_privs:
            os.chown(self._tmp_directory, 0, 0)
        self._postexit()

    def get_worker_additional_args(self) -> Optional[List[str]]:
        """
        Get the additional arguments for the selected mode to be
        added on the end of the arg list.
        """

        # Actually gets extra worker arguments ^.^
        selector = {
            PROGRAM_MODE.LIST_REGIONS: None,
            PROGRAM_MODE.MODIFY_CONFIG: lambda: [
                "--overwrite-config", self._tmpconfig
            ],
            PROGRAM_MODE.LIST_SELECTED_PEERSET: None,
            PROGRAM_MODE.LIST_SELECTED_PEERSET_NO_LOCALISE: None
        }

        extra_args = by_default(selector[self._mode], lambda: [])()
        
        if self._can_drop_privs:
            extra_args = extra_args + [
                    "--drop-to-uid", str(self._sudo_uid), "--drop-to-gid", str(self._sudo_gid)
            ]
        
        return extra_args




#######################################################################################
#################### HEURISTIC STRING-TYPE IDENTIFIERS ################################
#######################################################################################



def ipv4_heuristic_extractor(data: str, search_start_idx: int=0) -> Tuple[int, Optional[str]]:
    """
    Attempts to heuristically locate an IPv4 address in the string.

    Returns the index after the end of the ip address (len(data)) if none found

    If no candidates are found, then we return None as the IPv4 string.
    """

    ip_components = "0123456789."

    # We use these to estimate that we are at the end of an IPv4 address.
    ip_terminator_chars = "/:\\ ?\n\t" + string.whitespace


    # Look for the first sequence of #s with a dot and another number
    # after and eventually nothing.
    idx = search_start_idx

    # Keep going until the end
    while idx < len(data):
        # Try to find an initial number.
        while idx < len(data) and data[idx] not in string.digits:
            idx+=1
        
        if idx == len(data):
            return (idx, None)
        # Scan for the next numbers. Max # length is 3 digits
        init_idx = idx
        while idx < len(data) and idx < init_idx + 3 and data[idx] in string.digits:
            idx+=1
        if idx == len(data):
            return (idx, None)
        # Check if we are a dot, if not, then break and continue on.
        if not data[idx] == ".":
            continue
        idx+=1
        if idx == len(data):
            return (idx, None)
        # check if THIS is a number. If so we move on to see if we get to a breakout character.
        # and then assume this is an IP address.
        if data[idx] in string.digits:
            while idx < len(data) and data[idx] in ip_components:
                idx+=1
            if idx == len(data) or data[idx] in ip_terminator_chars:
                return (idx, data[init_idx:idx])
    return (idx, None)


def ipv6_heuristic_extractor(data: str, search_start_idx: int=0) -> Tuple[int, Optional[str]]:
    """
    Attempts to heuristically locate an ipv6 address in the string. 

    Returns the index after the end of the ip address if found, len(data).

    If none found then we return None as the IPv6 string.
    """
    # Note that finding the start of an IPv6 string is usually harder because it could
    # also be a colon.
    
    ip_segments = "0123456789abcdefABCDEF"
    ip_separators = ":"
    pre_ip_chars = "[" + string.whitespace + "/\\`'\"~;"
    ip_components = ip_segments + ip_separators
    ip_terminators = "\\/]`'\"~;" + string.whitespace

    # ipv6 has a max of 8 : separated components, each with max 4 hex digits.

    idx = search_start_idx

    while idx < len(data):
        # Try to find an ip initiator, or test front.
        while idx < len(data) and not ((idx == 0) or (data[idx] in pre_ip_chars)):
            idx += 1
        
        # Initiator is here
        if idx == len(data): # No initiator since end
            return (idx, None)
        idx += 1
        #First part of ip address is here.
        if idx == len(data): # No IP start since we're at end
            return (idx, None)
        init_idx = idx
        if data[idx] not in ip_segments: # Not an IP address.
            continue
        while idx < len(data) and data[idx] in ip_components:
            idx +=1
        # ^Locate the end of the prospective IP address. We are now at one-after
        # the end.

        # Check we are at a valid IP address end.
        if not (idx == len(data) or data[idx] in ip_terminators):
            continue

        # Pull out the actual IP.
        hypothetical_ip_addr: str = data[init_idx:idx]

        # Now we check the ip address, it's pretty simple.
        components = hypothetical_ip_addr.split(ip_separators)
        if len(components) > 8:
            continue
        if any(len(component) > 4 for component in components):
            continue

        return (idx, hypothetical_ip_addr)
    # Reached the end with no valid IPs.
    return (idx, None)











########################################################################################
#################### STRING MANIPULATION + GENERAL UTILITIES ###########################
########################################################################################



def string_sequence_replace(
        _initial_string: str, 
        character_sequence_components: str, 
        replacement: str=""
    ) -> str:
    """
    Take a string, remove any of `character_sequence_components` from the 
    start and end, and remove any of them from within the string too (optionally
    replaced by the replacement)

    e.g. ("void--_-time  -", "_- \t", "-~") -> "void-~time"
    """
    strn = _initial_string.strip(character_sequence_components)
    res = []
    idx = 0

    # Run through and pull out all of the subspace (heh) dividers from the string.
    while idx < len(strn):
        if strn[idx] in character_sequence_components:
            res.append(replacement)
            # Skip the rest rather than adding them
            while idx < len(strn) and strn[idx] in character_sequence_components:
                idx+=1
        else:
            res.append(strn[idx])
            idx+=1
    return ''.join(res)


def confirm(message: str, noconfirm: bool=False, default: bool=False, noconfirm_default: bool=True) -> bool:
    """
    Confirm whether or not to do an action.

    The message has [y/N] or [Y/n] appended to it depending on the default value (
    for invalid values). Returns whether the user confirmed (True) or denied (False)
    the operation.

    If noconfirm is True, then we just return the noconfirm_default value and print nothing.
    """
    response_lower_map: Dict[str, bool] = {
        "y": True,
        "yes": True,
        "1": True,

        "n": False,
        "no": False,
        "0": False
    }

    if noconfirm:
        return noconfirm_default
    else:
        i = input(message + " [" + ("Y" if default else "y") + "/" + ("n" if default else "N") + "]? ")
        i = i.strip().lower()
        if i not in response_lower_map.keys():
            return default
        else:
            return response_lower_map[i]

def nosymlinks_fileopener(fname, flags):
    """
    Opener for `open` that tries to pass the no follow symlinks flag.

    See: https://docs.python.org/3/library/os.html#os.O_NOFOLLOW
    and https://docs.python.org/3/library/functions.html?highlight=open#open
    """
    return os.open(fname, flags | os.O_NOFOLLOW)


def glue_all_files_together(filelist: Iterable[FilePath], file_prefix: FilePath, follow_symlinks:bool=False) -> str:
    """
    Glue all the files in the iterable together and put them in the output data, aka
    the return value.

    The file prefix is a prefix for every file. The files in the list will be normalised.
    Note that this DOES NOT PREVENT symlink issues, though we TRY to prevent those by 
    trying to pass os.O_NOFOLLOW
    """
    net_contents = ""
    for relative_filename in filelist:
        abs_file = os.path.join(
            file_prefix, 
            relative_filename
        )
        with open(abs_file, "rt", opener=nosymlinks_fileopener) as f:
            net_contents += f.read() + '\n'

    return net_contents


class ComparableWith(Protocol[T]):
    @abc.abstractmethod
    def __lt__(self, other: T) -> bool:
        pass
    @abc.abstractmethod
    def __gt__(self, other: T) -> bool:
        pass
    @abc.abstractmethod
    def __eq__(self, other: T) -> bool:
        pass






##################################################################################
########## INSTALL-AND-CONFIG ####################################################
##################################################################################



def is_executable_on_path(name: str) -> bool:
    """
    Returns if we find the given executable on the path
    """
    all_locations_to_check = os.get_exec_path()
    for path_dir in all_locations_to_check:
        yggpath = os.path.join(path_dir, name)
        if os.path.exists(yggpath) and (os.path.isfile(yggpath) or os.path.islink(yggpath)):
            return True

    return False

def is_yggdrasil_on_path() -> bool:
    """
    Sees if yggdrasil is on PATH or not.
    """

    return is_executable_on_path("yggdrasil")


def generate_nonexistent_filename(unformatted_path: str) -> str:
    """
    Makes us able to continue to generate new filenames until one which doesn't 
    exist already is found.

    unformatted_path is a format string that should take {ext} as the string "-1, -2, -3, ...."
    if, when formatted with ext="", the file exists.

    For example yggdrasil{ext} would create the file yggdrasil if there was no file with
    that name, then yggdrasil-1, then yggdrasil-2, etc.
    """

    filename = unformatted_path.format(ext="")
    num = 0
    while os.path.exists(filename):
        num += 1
        filename = unformatted_path.format(ext="-" + str(num))

    return filename

def install_pacman():
    """
    Installs yggdrasil with pacman.
    """
    subprocess.run(["sudo", "pacman", "-S", "yggdrasil"])

GPG_KEY_ID = "569130E8CA20FBC4CB3FDE555898470A764B32C9"
URL_BASE = "https://neilalexander.s3.dualstack.eu-west-2.amazonaws.com/"
GPG_KEY_LOCATION = URL_BASE + "deb/key.txt"

DEB_REPO = URL_BASE + "deb/"
RPM_REPO = URL_BASE + "rpm/"

def fetch_keys():
    """
    Fetch keys into local gpg keyring
    """
    subprocess.run(["gpg", "--fetch-keys", GPG_KEY_LOCATION])


def install_apt_get():
    """
    Set up repos and install the package with apt-get.

    Note that because I run arch I have no good way to personally test this rn
    (until I set up some VMs :p)

    This sets up https://linux.die.net/man/5/sources.list /etc/apt/sources.list.d
    if it does not exist.

    See: https://yggdrasil-network.github.io/installation-linux-deb.html
    """
    subprocess.run(["sudo", "mkdir", "-p", "/etc/apt/sources.list.d"])
    # Check for dirmngr, needed for raspi (it is used by gpg) 
    if not is_executable_on_path("dirmngr"):
        subprocess.run(["sudo", "apt-get", "install", "dirmngr"])
    # get gpg keys to local keyring
    fetch_keys()
    # Export them to apt-key
    subprocess.run(["gpg --export " + GPG_KEY_ID + " | sudo apt-key add -"], shell=True)
    apt_repo_source_list = generate_nonexistent_filename(
            "/etc/apt/sources.list.d/yggdrasil{ext}.list"
    )
    subprocess.run(["echo 'deb " + DEB_REPO + " debian yggdrasil' | sudo tee " + apt_repo_source_list], shell=True)
    subprocess.run(["sudo", "apt-get", "update"])
    subprocess.run(["sudo", "apt-get", "install", "yggdrasil"])



def install_rpm():
    """
    Set up repos and install the package with dnf. 

    Also sets up system account and *stuff*

    See: https://yggdrasil-network.github.io/installation-linux-rpm.html
    """
    fetch_keys()
    # Attempt to generate a keyring file address in rpm...
    rpm_keyring_file = generate_nonexistent_filename(
            "/etc/pki/rpm-gpg/RPM-GPG-KEY-yggdrasil{ext}"
    )

    # Export the fetched key to that keyring file
    subprocess.run([
        "gpg --armor --no-comment --export-options export-minimal --export " + 
        GPG_KEY_ID + " | sudo tee " + rpm_keyring_file
    ], shell=True)

    subprocess.run(["sudo", "mkdir", "-p", "/etc/yum.repos.d"])
    # Generate the .repo file in /etc/yum.repos.d
    ygg_repo = generate_nonexistent_filename(
            "/etc/yum.repos.d/yggdrasil{ext}.repo"
    )

    repo_contents = """\
[yggdrasil]
name = Yggdrasil
baseurl = {rpm_repo}
gpgcheck=1
gpgkey=file://{rpm_keyring_file}
""".format(rpm_repo=RPM_REPO, rpm_keyring_file=rpm_keyring_file)
    subprocess.run(["echo '" + repo_contents + "' | sudo tee " + ygg_repo], shell=True) 
    
    # create the system group
    subprocess.run(["sudo", "groupadd", "--system", "yggdrasil"])
    # Install yggdrasil!
    subprocess.run(["sudo", "dnf", "install", "yggdrasil"])


def systemd_enable():
    subprocess.run(["sudo", "systemctl", "--system", "enable", "yggdrasil"])

def systemd_start():
    subprocess.run(["sudo", "systemctl", "--system", "start", "yggdrasil"])


# Detect the packager type - apt-based, pacman-based, or dnf-based.
# Note that Termux on android also has apt available but also a pkg
# wrapper and specific different directory structure that we can detect in the 
# future (in particular, <tmux>/home and <tmux>/usr are the only ones present and <tmux>/usr 
# contains all the usual /etc, /var, etc.


@enum_argparse
@enum.unique
class SYSPKG_TYPE(enum.Enum):
    """
    We have 3 main packaging types... Android tmux maybe in future?

    PACMAN - arch, manjaro, etc.
    APT - debian, ubuntu, etc.
    DNF - fedora, etc.
    """
    PACMAN = enum.auto()
    APT = enum.auto()
    DNF = enum.auto()


def detect_packager_type() -> Optional[SYSPKG_TYPE]:
    """
    Attempts to detect the system packaging type by looking for pacman, apt, etc.
    """
    if is_executable_on_path("pacman"):
        return SYSPKG_TYPE.PACMAN
    if is_executable_on_path("apt-get"):
        return SYSPKG_TYPE.APT
    if is_executable_on_path("dnf"):
        return SYSPKG_TYPE.DNF


def get_install_function(systype: Optional[SYSPKG_TYPE], errmessage: str) -> Callable[[], None]:
    """
    Gets an install function for the given system type.

    If we cannot find one, then we return a function that errors out of the program
    with the given error message.
    """
    mapper = {
        SYSPKG_TYPE.PACMAN: install_pacman,
        SYSPKG_TYPE.APT: install_apt_get,
        SYSPKG_TYPE.DNF: install_rpm
    }

    if systype in mapper.keys():
        return mapper[systype]
    else:
        def quiterr():
            print(errmessage, file=sys.stderr)
            sys.exit(1)
        return quiterr


class YggdrasilInstalledContext(contextlib.AbstractContextManager):
    """
    A context in which yggdrasil is installed, though you can force no checks.

    This automatically starts yggdrasil if it wasn't installed, at the end.
    """
    def __init__(self, install: bool=True, packager_type_override: Optional[SYSPKG_TYPE]=None):
        self._do_install = install
        self._was_initially_installed = False
        self._pkg_type_override = packager_type_override
        # True when the install was done
        # used to avoid weirdness on exceptions in the __exit__
        self._done_install = False

    def __enter__(self):
        if self._do_install and not is_yggdrasil_on_path():
            # Then we do install!
            self._system_type = detect_packager_type() if self._pkg_type_override is None else self._pkg_type_override
            install_function = get_install_function(
                    self._system_type, 
                    "Failed to detect system type to install yggdrasil but there is no installed yggdrasil"
            )
            install_function()
            systemd_enable()
            self._done_install = True

        else:
            self._done_install = True
            self._was_initially_installed = True
        return self

    def __exit__(self, *exc):
        if not self._was_initially_installed and self._done_install and self._do_install:
            systemd_start()
        return None


def needs_yggdrasil_installed(mode: PROGRAM_MODE) -> bool:
    """
    Returns if the given mode needs an actual yggdrasil install
    """
    results = {
        PROGRAM_MODE.LIST_REGIONS: False,
        PROGRAM_MODE.LIST_SELECTED_PEERSET: False,
        PROGRAM_MODE.LIST_SELECTED_PEERSET_NO_LOCALISE: False,
        PROGRAM_MODE.MODIFY_CONFIG: True
    }

    return results[mode]










##################################################################################
########## AUTOCONFIG ############################################################
##################################################################################



# This function generates a region list for each timezone according to the value returned
# by time.timezone - the non-daylight-saving timezone offset in seconds. It is converted
# to days
def generate_timezone_region_list(tmzone: float) -> List["Region"]:
    """
    Generates a region list for a timezone as returned by time.timezone - which 
    returns "seconds west of UTC in non Daylight Savings (a cursed idea, DST was) time".

    It is autoconverted to rough days. Note that this is a WIP list. I am NOT writing out
    all the countries everywhere >.>

    STANDARD REGIONALISING FORMAT for simple directions:
        <region>[-{east/west/centre/none}-{north/south/centre/none}]/...

        specifically if there is no reasonable value for any of these orientations
        then you can put none.

    This is so we can use per-component prefixing for easy matching ^.^
    """
    days_east_of_utc = -tmzone/SECONDS_PER_DAY  # We make western europe +ive here

    res = []

    if -2 < days_east_of_utc < 2:
        res.extend([  # TODO change all this stuff with east/west europe.
            "/europe-west-north/uk", 
            "/europe-west-north/united-kingdom", 
            "/europe-west-north/gb", 
            "/europe-west-north/ireland",
            "/europe-west-south/spain",
            "/europe-west-centre/france",
            "/europe-west-south/portugal",
            "/europe-west-north/iceland",
            "/europe-centre-centre/germany",
            "/europe-west",
            "/africa-west",
            "/europe-west-centre/netherlands"
        ])
    if 2 <= days_east_of_utc < 4:
        res.extend([
            "/europe-centre-centre/germany",
            "/europe-centre-south/italy",
            "/europe-east-centre/poland",
            "/europe-east-south/turkey",
            "/europe-centre-north/sweden",
            "/africa-east",
            "/europe-east",
            "/europe-east-centre/ukraine",
            "/europe-centre-centre/switzerland",
            "/europe-east/slovakia",
            "/europe-east/russia-west",
            "/europe-east/bulgaria",
            "/europe-east/romania"
        ])
    if 4 <= days_east_of_utc < 10:
        res.extend([
            "/asia/china",
            "/asia-east/taiwan",
            "/asia-none-north/russia-east",
            "/europe-east/russia-east",
            "/asia-west-south/india",
            "/asia-east/singapore",
            "/asia-east/hongkong",
            "/oceania/australia",
            "/asia-east-south/malaysia",
            "/asia-east-south/papua-new-guinea"
            "/asia-east-south/indonesia"
        ])
    if 10 <= days_east_of_utc <= 12 or -12 <= days_east_of_utc < -9:
        res.extend([
            "/america-west-north/united-states-west/alaska",
            "/north-america/united-states", # Legacy
            "/asia-east-centre/japan",
            "/oceania-east-centre/new-zealand",
        ])
    if -9 <= days_east_of_utc < -6:
        res.extend([
            "/north-america/united-states-west", # Legacy
            "/north-america/canada-west",  # Legacy
            "/america-west-north/united-states-west",
            "/america-west-north/canada-west",
            "/america-none-centre/mexico",
        ])
    if -6 <= days_east_of_utc <= -2:
        res.extend([
            "/america-east-north/canada-east"
            "/america-east-north/united-states-east",
            "/america-east-south/brazil",
            "/america-west-south/chile",
            "/america-none-south/ecuador",
            "/america-none-south/argentina",
            "/north-america/united-states-east",  # legacy
            "/north-america/canada-east", # legacy
            "/south-america/brazil", # legacy
            "/south-america/ecuador", # legacy
            "/europe-west-north/greenland"
        ])
    return res










##############################################################################
############# REGION MANIPULATION ############################################
##############################################################################



# simple enum for auto region detection.
@enum.unique
class REGION_DETECT(enum.Enum):
    """
    Single element tag enum indicating we should automatically detect region.
    """
    AUTOREGION = enum.auto()


@enum_argparse
@enum.unique
class REGION_SELECT(enum.Enum):
    """
    Enum determining how to select peers from non-automatic regions specified on the command line.
    """
    UNIFORM_BY_REGION = enum.auto()  # Select peers randomly, where each peer picked has an equal chance of coming from any region
    UNIFORM_BY_PEER = enum.auto()  # Select peers randomly, where each peer has an equal chance, 
    # ignoring region of origin (so bigger regions have more chance of their peer nodes getting selected)
    PRIORITY_BY_REGION = enum.auto()  # Select peers in order of region - i.e. pull out peers from the first added region until none meet 
    # the criteria, then go to the next, and so on
    PRIORITY_BY_DEPTH = enum.auto()  # Select uniform-by-region-ly for all regions of a given depth until there are no more peers there.
    # Then move to the next lowest depth and do the same, until end. 


def normalise_region(_region_identifier: Region, noendnorm: bool=False) -> Region:
    """
    Just normalises a region path with os.path.normpath for preemptive region listing.

    Note that regions are case-insensitive, and any ambiguity in region cases includes
    *all* region directories and files which match them. Furthermore, regions include
    *all* files for which the normalised region string is a prefix, so
    "/eur", would match "/EuRope", "/euro", "/europe/uK.md", etc.

    We also convert all sequences of spaces and tabs and underscores and dashes in regions to single
    dashes - except at the start and end where we strip all spaces

    You can specifically set noendnorm = True to ENABLE keeping the end slash, which 
    should be done for user-specified paths in case they want to see all the regions
    INSIDE another one.

    This will also catch the /. from using relpath, and turn it into /
    """
    npath = os.path.normpath(_region_identifier).lower()
    had_slash = _region_identifier.endswith('/')

    subspace_dividers: str = string.whitespace + "-_"  # k i n k y

    subzones = npath.split('/')
    subzones = [
            string_sequence_replace(zone, subspace_dividers, '-') 
            for zone in subzones
    ]
    npath = '/'.join(subzones)

    if npath == "/.":
        return Region("/")

    if not npath.startswith("/"):
        npath = '/' + npath

    if npath.endswith('/') and (not npath == '/') and (not noendnorm):
        npath = npath[:-1]
    
    # Counteract any slash removal os path does in the case of no end normalisation.
    if noendnorm and had_slash and not npath.endswith('/'):
        npath += '/'

    return Region(npath)


def join_regions(region: Region, *regions, noendnorm:bool=False) -> Region:
    """
    Join regions together. Note that this does not guaruntee the resulting region exists,
    it just joins regions and renormalises them.
    
    We preserve endslashes if noendnorm is True.
    """
    # I think os.path.join gets rid of trailing slashes.
    unchecked_result = normalise_region(os.path.join(region, *regions), noendnorm=noendnorm)

    if noendnorm:
        last_had_slashes = [region, *regions][-1].strip().endswith('/')
        if last_had_slashes:
            unchecked_result += '/'

    return Region(unchecked_result)

def is_subregion(_region_a: Region, _superregion_b: Region, noendnorm:bool=False) -> bool:
    """
    Return if region a is a (non-strict i.e. could be equal) subregion of region b.

    We do normalisation and per-element prefix checking.

    Note that /europe and /europe/ ARE interpreted as the same here unless noendnorm is true
    (in which case /europe is not a subregion of /europe/
    """
    if _superregion_b == "/":
        return True
    region_a: Region = normalise_region(_region_a)
    region_b: Region = normalise_region(_superregion_b)

    components_a = region_a.split('/')
    components_b = region_b.split('/')

    if len(components_a) < len(components_b):
        return False  # Definitely not a subregion, has fewer components.
    else:  # check prefix-wise.
        return all(components_a[i].startswith(components_b[i]) for i in range(len(components_b)))

def region_depth(_region: Region, noendnorm:bool=False) -> int:
    """
    Get the canonical depth of the given region.

    Essentially a normalisation and then count the slashes, but / gives
    0 depth, unless noendnorm is given, which will allow ending slashes to be preserved.
    """
    region = normalise_region(_region, noendnorm=noendnorm)
    return 0 if (region == "/" and not noendnorm) else region.count('/')


@enum_argparse
@enum.unique
class PEER_LIST_LOCATION_MODE(enum.Enum):
    """
    Holds the type of the location of the list of enums.
    """
    GIT = enum.auto()
    DIRECTORY = enum.auto()
    FILE = enum.auto()


def exclude_region(region: Region) -> bool:
    """
    exclude regions based on certain filenames.

    In particular, we generally exclude readme and license
    regions.
    """
    excl_set = {'readme', 'license'}
    return region.split('/')[-1] in excl_set










#######################################################################################
##################### REGION LIST MANIPULATION ########################################
#######################################################################################



def order_region_data(
        regions: Dict[Region, V], 
        noendnorm: bool=False
    ) -> Tuple[
        OrderedRegionDataCollection[V], 
        List[int]
    ]:
    """
    This takes a preexisting region list, and sorts it using a particular ordering,
    such that it is efficient to find all elements with a first element with a given
    region prefix - the largest available region set with the prefix.

    Note that the returned first list also includes the slashcount too.

    The second returned list is JUST a list of slashcount to be used with `bisect`
    (since there is no convenient keyfunction there, for the exact purpose of caching
    keys like this)

    In particular, the list is sorted in order of the depth
    and then the length (in characters) of the prefix. Since the region dictionary produces
    all possible regions for each # of separators
    You can also control whether or not end-slashes are removed when calculating depths.

    This is good for when dealing with listings where you may want /.../ to locate 
    all the subregions rather than only the main region shown.
    """
    base_list = [
        (region, region_depth(region, noendnorm=noendnorm),  data) 
        for region, data in regions.items()
    ]

    #def ordering_function_lessthan(a, b):
        #ra, slashes_a, _ = a
        #rb, slashes_b, _ = b

        #if slashes_a < slashes_b:
        #    return True
        #elif slashes_a > slashes_b:
        #    return False
        #else: # equal # of slashes
        #    return len(ra) < len(rb)

    # Use a key function and tuple-lexicographical-sorting rather than a comparator

    def keyordering(v):
        return (v[1], len(v[0]))
    
    base_list.sort(key=keyordering)
    return base_list, [p[1] for p in base_list]


def get_data_for_broadest_contained_regions( 
        containing_region: Region, 
        ordered_regions: OrderedRegionDataCollection[T], 
        depths: Optional[List[int]]=None, 
        optimise_root:bool=True,
        noendnorm: bool=True
    ) -> List[Tuple[Region, T]]:
    """
    Using an ordered set of regions (like produced by order_region_data), find all the broadest
    contained regions of the containing region in the given dataset.

    This can take a key argument like that produced by the order_regions() function.

    When this locates for a given region, it always by default does not remove any extra
    slashes - this is so /europe/ can spit out all subregions of europe without including
    europe itself. 

    If we are simply /, then we don't bother with anything else lol. Unlike the other one above there is
    no reason to even bother (the other one at least means you can see possible regions)
    """
    # Find the first index with the depth. If this is the end of the list 
    # then none were found.
    containing_region = normalise_region(containing_region, noendnorm=noendnorm)
    depths = get_depths(ordered_regions, depths)

    if optimise_root and containing_region == "/":
        if len(ordered_regions) > 0:
            curr_region, _, curr_data = ordered_regions[0]
            return [(curr_region, curr_data)]
        else:
            return []

    our_depth = region_depth(containing_region, noendnorm=noendnorm)

    idx = bisect.bisect_left(depths, our_depth)
    if idx >= len(ordered_regions):
        return []

    results = []

    # Else, we scan through until either we hit the end or the slashcount increases.
    while idx < len(ordered_regions) and depths[idx] == our_depth:
        curr_region, _, curr_data = ordered_regions[idx]
        if is_subregion(curr_region, containing_region):
            results.append((curr_region, curr_data))
        idx += 1

    return results


def get_depths(
        regions: OrderedRegionDataCollection[T], 
        depths: Optional[List[int]]=None
    ) -> List[int]:
    """
    Calculate depths if not provided.
    """
    return depths if depths is not None else [p[1] for p in regions]


def get_broadest_contained_regions( 
        containing_regions: List[Region], 
        ordered_regions: OrderedRegionDataCollection[T], 
        depths: Optional[List[int]]=None,
        noendnorm:bool=True
    ) -> Set[Region]:
    """
    Get all the most broad - so no /europe/ffff for /europe (but yes for /europe/ if noendnorm 
    is true) regions that are contained within the specified region_prefixes. Prefixes are automatically 
    normalised to prefix/noend-normalisation.

    Note that this is per-region so if, say /europe/ffff and /europe are in the region prefixes, you might get 
    /europe-west/ffff and /europe-west
    """
    result = set()
    depths = get_depths(ordered_regions, depths)

    for rcontainer in containing_regions:
        sets_of_contained_regions = get_data_for_broadest_contained_regions(rcontainer, ordered_regions, depths, optimise_root=False, noendnorm=noendnorm)
        for contained_region, _ in sets_of_contained_regions:
            result.update([contained_region])

    return result

def get_all_broadest_contained_region_data( 
        containing_regions: List[Region], 
        ordered_regions: OrderedRegionDataCollection[Iterable[T]], 
        depths: Optional[List[int]]=None,
        noendnorm: bool=True
    ) -> Set[T]:
    """
    Get all the data for regions contained in region_prefixes, using no-end
    normalisation (changeable), and combine them into a set of overall results.

    You can easily control whether or not the region prefixes distinguish between
    /... and /.../ with noendnorm.

    (in the case of filedata)
    Remember that all of these files are probably relative to the specified base
    directory if you used the previously defined functions to generate the lists.
    """
    result = set()

    depths = get_depths(ordered_regions, depths)

    for containing_region in containing_regions:
        contained_regions = get_data_for_broadest_contained_regions(
            containing_region, 
            ordered_regions, 
            depths=depths, 
            noendnorm=noendnorm
        )
        for _, data in contained_regions:
            result.update(data)

    return result

def get_containing_regions_and_data(
        _contained_regions: List[Region],
        depth_ordered_paths: OrderedRegionDataCollection[T]
    ) -> OrderedRegionDataCollection[T]:
    """
    This selects all data from regions that contain the specified contained regions, of course,
    returning them in increasing depth order (as expected by the type).

    Running backwards over the result of this will give you, in effect, a "fallback path" if you
    are using this to pull peers, for when you can't get enough from the most precise regioning.
    """
    # normalise paths
    contained_regions = [normalise_region(path) for path in _contained_regions]

    def is_included(depth_ordered_path: Tuple[Region, int, T]) -> bool:
        """
        Just checks if it is a prefix of any of the paths to cover i.e. is a valid
        region covering that.
        """
        return any(is_subregion(selector_path, depth_ordered_path[0]) for selector_path in contained_regions)

    # This will already be sorted since the depth ordered paths are sorted.
    return list(filter(is_included, depth_ordered_paths))


def parse_region_data( 
        region_data: OrderedRegionDataCollection[T], 
        transform: Callable[[T], V]
    ) -> OrderedRegionDataCollection[V]:
    """
    Transforms region data while maintaining associated regions.
    """
    return [
        (r, s, transform(data)) 
        for r, s, data in region_data
    ]











########################################################################################
###################### PEER LISTING AND MANIPULATION ###################################
########################################################################################



@enum_argparse
@enum.unique
class PEER_MODE(enum.Enum):
    """
    Peer connection mode.

    The enum values here are the prefixes in the URI <value>/,
    (since tor and i2p nodes aren't bare IPs/urls in a URI)

    with the local tor port being {torport} formattable and the local 
    i2p port being {i2pport}, and the tor host is {torhost} and the i2phost
    is {i2phost}. By default the latter two are localhost, i.e. you're running
    your traffic through a node running on your own machine.

    We effectively find tor and i2p peers by selecting using their default listing

    (for tor this is {torhost=localhost} and {torport=9050}, as these are how they are
    listed in public listings) - (for i2p its {i2phost=localhost} and {i2pport=4447}
    """
    TCP = 'tcp:/'
    TLS = 'tls:/'
    TOR = 'socks://{torhost}:{torport}'
    I2P = 'socks://{i2phost}:{i2pport}'


GenPeerAddr = Tuple[PEER_MODE, str]
# Like the above but containing parsed lists of peers instead of filenames.
OrderedRegionedPeerCollection = OrderedRegionDataCollection[Set[GenPeerAddr]]



def gen_detection_prefixes(mode: PEER_MODE) -> Set[str]:
    """
    Generate all the detection prefixes for each peer mode. Basically runs through
    every combo of the four parameters specified by the default possible values.
    """
    generic_prefix = mode.value
    prefixes = set()
    defaults = [DEFAULT_TOR_NODE_HOSTS, DEFAULT_TOR_PORTS, DEFAULT_12P_NODE_HOSTS, DEFAULT_I2P_PORTS]
    for torhost, torport, i2phost, i2pport in itertools.product(*defaults):
        kwargs = {"torhost": torhost, "torport": torport, "i2phost": i2phost, "i2pport": i2pport}
        prefixes.add(generic_prefix.format(**kwargs))

    return prefixes
    

def gen_all_detection_prefixes() -> Dict[PEER_MODE, Set[str]]:
    """
    Generate prefixes for detecting each peer type inside lists of config files.
    """
    return {pm: gen_detection_prefixes(pm) for pm in list(PEER_MODE)}
    
# Easy constant dictionary generation

nodetype_detection_prefixes = gen_all_detection_prefixes()
inverse_nodetype_det_pre: Dict[str, PEER_MODE] = {}

for k, vset in nodetype_detection_prefixes.items():
    for v in vset:
        inverse_nodetype_det_pre[v] = k


def detect_peer_uri(line: str) -> Optional[GenPeerAddr]:
    """
    Attempts to detect a URI for a peer in the given line. If it finds one,
    then we return a pair (PEER_MODE, postfix), where postfix is the part of the
    address AFTER the prefix in PEER_MODE enum. so for tcp it would be /a.b.c.d:port
    since the full address would be tcp://a.b.c.d:port ~ This is so we can localise.

    Because the format files come in a wide variety of formats, we have to try and be
    pretty dynamic here. First thing we will try is trying to find a prefix anywhere in the
    line, and picking the first one (so if for some reason an address must contain another
    internal one it won't cause issue), then running up until some sensible end characters.


    """

    end_chars = "`'\"" + string.whitespace

    # Minimum start location of the indexing substring, the prefix used, and the PEER_MODE
    # that specific prefix indicates. Default vals are just something to compare with.
    min_set = (len(line), '', None)

    for prefix in inverse_nodetype_det_pre.keys():
        smallest_idx = line.find(prefix)
        if smallest_idx != -1:  # We actually found something
            if smallest_idx < min_set[0]:
                min_set = (smallest_idx, prefix, inverse_nodetype_det_pre[prefix])
                
    if min_set[0] == len(line):
        return None

    idx = min_set[0]
    # Else we can now run through the address.
    # First, skip past the prefix...
    idx += len(min_set[1])

    # Now scan through until we hit an end character or the end of the string.
    endidx = idx

    while endidx < len(line) and line[endidx] not in end_chars:
        endidx+=1

    # Now we have our address suffix ^.^

    return min_set[2], line[idx:endidx].rstrip()

def scan_for_peer_uris(long_text:str) -> List[GenPeerAddr]:
    """
    Scan through every line of the text. We actually also split on all spaces and
    newlines too.
    """
    lines = long_text.splitlines()
    elements = []
    for line in lines:
        elements.extend(line.split())
    
    uris = []
    for e in elements:
        res = detect_peer_uri(e)
        if res is not None:
            uris.append(res)

    return uris

def localise_peer_uri(
        generalised_peer_address: GenPeerAddr, 
        torhost=DEFAULT_TOR_NODE_HOSTS[0],
        i2phost=DEFAULT_12P_NODE_HOSTS[0],
        torport=DEFAULT_TOR_PORTS[0],
        i2pport=DEFAULT_I2P_PORTS[0]
    ) -> str:
    """
    A generalised method to localise (so to speak) a generalised peer address
    (with auto-detected protocol and a suffix independent of local conditions)
    by setting up the resulting specific address with the appropriate ports 
    and IP for interfacing with where the peer is. e.g. for peers on Tor, it
    would take a generalised address and point it to the node running where 
    you specified.
    """
    # Note that we specifically ONLY localise the prefix so we can avoid injection
    # attacks by adding, say, a "{torhost}" in the actual address and sending that
    # IP to an attacker in the malicious repo or file. There's probably a way around
    # this but the way we extract this in the first place (i.e. dump this as the actual
    # address in the config file) means it will always be a prefix and that makes it
    # much harder to exploit for tor/i2p deanonymisation if the user is running a tor
    # or i2p node on a VPS or something (though that seems like an obscure usecase, all
    # this stuff is usually just on "localhost" anyhow.
    prefix = generalised_peer_address[0].value.format_map({
        "torhost": torhost,
        "i2phost": i2phost,
        "torport": torport,
        "i2pport": i2pport
    })
    suffix = generalised_peer_address[1]
    return prefix + suffix










######################################################################################
############### PEER FILTERING #######################################################
######################################################################################



class Filter(object):
    """
    Simple class for filters.

    Set name to None to not enter into the registry.
    """
    filter_registry: ClassVar[Dict[PEER_MODE, Dict[str, "Filter"]]] = {a: {} for a in list(PEER_MODE)}
    def __init__(self, name: Optional[str],  base_filter: Callable[[GenPeerAddr], bool], categories: Set[PEER_MODE]):
        """
        Create a filter and register it with the overall set of filters.
        """
        super().__init__()

        self._base_filter = base_filter
        self._categories = categories
        self._name = None
        self.register(name)
        
        # On plain call is not writeable. But we can write to .__func__ which is
        # where it actually gets docstring from anyway
        self.__call__.__func__.__doc__ = self._base_filter.__doc__

    def __call__(self, peer: GenPeerAddr) -> bool:
        if peer[0] in self._categories:
            return self._base_filter(peer)
        else:
            return True

    def __and__(self, other: "Filter") -> "Filter":
        """
        We use & and | to logically combine filters :p

        (we can't redefine and and or since those do boolean things).

        Note that our boolean operations are smart about categories. 

        In particular, when within any given category, we evaluate solely the filters
        within that category (or the or function would be near useless).

        This works without weirdness in & because Filters produce True outside their
        bounds.
        """
        if not isinstance(other, Filter):
            return NotImplemented
        # Have to check for any category in which 
        return Filter(None, lambda a: self(a) and other(a), self._categories | other._categories)

    def __or__(self, other: "Filter") -> "Filter":
        """
        Specified as above - this will evaluate over OR at for each filter IF the category
        of the peer is that of the filter. If we are outside the categories, however, 
        then we do nothing.
        """
        if not isinstance(other, Filter):
            return NotImplemented
        # Select only the f which is within the region.
        return Filter(None, lambda peer: any((f(peer) for f in [self, other] if a in f._categories)), self._categories | other._categories)

    def __invert__(self) -> "Filter":
        """
        Simply takes the inversion of this filter.
        """
        return Filter(None, lambda peer: not self._base_filter(peer), self._categories)
    
    def register(self, name: Optional[str]) -> "Filter":
        """
        Register this filter with a name.

        Note that if the filter is already registered, this does not work.

        If name is None this does nothing.

        Returns ourself.
        """
        if self._name is not None:
            return NotImplemented
        if name is None:
            return self

        self._name = name
        for category in self._categories:
            Filter.filter_registry[category][name] = self

        return self

def apply_peer_filter(peerset: Set[GenPeerAddr], fil: Filter) -> Set[GenPeerAddr]:
    """
    Apply a filter across a given set. This generally assumes that the set has been 
    prefiltered for the categories of the filter applied, though it is completely
    unrequired (the Filter will just let all peers outside it's category range into the
    output set)
    """
    return set(p for p in peerset if fil(p))

def or_filters(fil: Filter, *fils: List[Filter]) -> Filter:
    """
    Combine filters together via the | operator in an efficient way 
    (to avoid Stack Overflows), by doing "logarithmic ORing"
    """

    overall = [fil, *fils]
    size = len(overall)
    if size == 1:
        return overall[0]
    elif size % 2 == 0:  # Even, just split down the middle :p
        middle = len(overall)/2
        return or_filters(*overall[0:middle]) | or_filters(*overall[middle:size])
    else:  # Odd, split off the end then split in the middle.
        end = overall.pop()
        size = len(overall)
        middle = size/2
        return or_filters(*overall[0:middle]) | or_filters(*overall[middle:size]) | end

def peer_filter(
        name: Optional[str], 
        peermodes: Set[PEER_MODE], 
        prerequisites: Optional[List[Filter]]=None
    ) -> Callable[[Callable[[GenPeerAddr], bool]], Callable[[GenPeerAddr], bool]]:
    """
    Filter for peers of a certain set of types.

    The function in question should take a parameter GenPeerAddr and return true if 
    the thing matches the filter. The GenPeerAddress will be of the given type (peer_filter return will filter out stuff that is irrelevant).

    The name can be set to None to not register it.

    You can also add prerequisites. Remember that if you provide filters, then outside of the networks they do not apply.
    """
    def fil(the_filter: Callable[[GenPeerAddr], bool]) -> Filter:
        """
        Filter function.
        """
        if prerequisites is None:
            return Filter(name, the_filter, peermodes)
        else:
            proper_filter = Filter(None, the_filter, peermodes)
            def prerequisited_fil(peer_addr: GenPeerAddr) -> bool:
                has_prereq = all(f(peer_addr) for f in prerequisites)
                return has_prereq and proper_filter(peer_addr)
            return Filter(name, prerequisited_fil, peermodes)

    return fil

# Finds if the given address is likely an ipv4 address (very approximate)
@peer_filter("ipv4", {PEER_MODE.TCP, PEER_MODE.TLS})
def ipv4(peer: GenPeerAddr) -> bool:
    """
    Pick out TLS and TCP addresses that are ipv4. Note that this will not recognise URLS/DNS.
    
    This is a rough, approximate filter, not super precise.
    """
    postfix = peer[1]

    return (ipv4_heuristic_extractor(postfix)[1] is not None)

   
# Finds if the given peer is at an IPv6 address. Significantly less approximate than
# the IPv4 heuristic.
@peer_filter("ipv6", {PEER_MODE.TCP, PEER_MODE.TLS})
def ipv6(peer: GenPeerAddr) -> bool:
    """
    Check if said peer is probably ipv6. Rough filter but decent ^.^
    """
    postfix = peer[1]

    return (ipv6_heuristic_extractor(postfix))[1] is not None

# Finds if the node is a dns-addressed node. Basically just requires that it is not just ipv4 or ipv6
dns = (~ipv4 & ~ipv6).register("dns")








##########################################################################
################## ARGUMENT PARSING ARGPARSE #############################
##########################################################################



# Argparse automatically fixes indents and words and stuff.
def parse_args(args) -> argparse.Namespace:
    """
    Actually parses arguments.

    If the FIRST ARGUMENT (element 0, since this does not take raw sysargs but 
    sysargs with the program name stripped off) is --work, then this marks this as
    having various extra arguments for allowing config overrides, as well as a .work
    member set to True.
    """
    aparser = argparse.ArgumentParser(
        prog="yggautoconf.py",
        description="""
            Provides a nice, automatic yggdrasil config generator that pulls from the generally
            maintained list of public peers on `github`.

            Note that this program will renormalise your yggdrasil config during the
            process.
        """,
        # formatter_class=argparse.RawTextHelpFormatter, TODO: write one which does what I want
        allow_abbrev=False # Future and script-proofing
    )

    aparser.add_argument("-y", "--noconfirm", 
        action="store_true",
        default=False,
        dest="noconfirm",
        help="Tell the program to just go through all the confirmations required",
    )

    aparser.add_argument("-l", "--initial-peer-list-location",
        default="https://github.com/yggdrasil-network/public-peers.git",
        dest="peerlist_location",
        help="""The location to get a directory containing files containing initial
        peers to select from. Default is the community-maintained git repo of public peers"""
    )

    aparser.add_argument("-t", "--initial-peer-list-type",
        default=PEER_LIST_LOCATION_MODE.GIT,
        dest="peerlist_location_type",
        type=PEER_LIST_LOCATION_MODE.ap_type,
        choices=PEER_LIST_LOCATION_MODE.ap_choices,
        help="""Tell the script whether the peer list location specified is a git repository
        or just a file directory."""
    )

    aparser.add_argument("-f", "--config-file",
        default="/etc/yggdrasil.conf",
        dest="config_file",
        help="""The yggdrasil config file to work with. You should be able to both
        read and write this file.
        """
    )

    aparser.add_argument("-p", "--protocol",
        default=[],
        action="append",
        type=PEER_MODE.ap_type,
        choices=PEER_MODE.ap_choices,
        dest="peer_protocols",
        help="""Specify different protocols for the nodes to use. 
        This option can be used repeatedly to select peers on multiple protocols.
        Default is TLS and TCP, if unspecified."""
    )

    yggdrasil_install_grp = aparser.add_argument_group(title="Yggdrasil Auto-Install Parameters", description="""
        Parameters for installing (or not installing) yggdrasil automatically from a detected package manager
        if it is not installed.
    """)

    yggdrasil_install_grp.add_argument("--no-auto-install",
        default=True,
        action="store_false",
        dest="auto_install_yggdrasil",
        help="""Tell the program to not even consider actually installing yggdrasil if it is
        unavailable-but-needed.
        """
    )

    yggdrasil_install_grp.add_argument("--force-packager-type",
        default=None,
        action="store",
        type=SYSPKG_TYPE.ap_type,
        choices=SYSPKG_TYPE.ap_choices,
        dest="packager_type_override",
        help="""Force the script to, in the case of lacking an installed yggdrasil,
        use the install method for the given packaging system as opposed to the detected
        packaging system.
        """
    )
    
    proxy_param_grp = aparser.add_argument_group(title="Proxy Parameters", description="""
        Parameters for modifying peers which use proxying services (i2p and tor) for
        your local setup.
        """)

    proxy_param_grp.add_argument("--tor-host",
        default=DEFAULT_TOR_NODE_HOSTS[0],
        type=str,
        dest="tor_host",
        help="Specify the location of the tor node you want to connect to peers on tor with"
    )

    proxy_param_grp.add_argument("--tor-port",
        default=DEFAULT_TOR_PORTS[0],
        type=port,
        dest="tor_port",
        help="Specify the port of the tor node to access peers on tor with."
    )

    proxy_param_grp.add_argument("--i2p-host",
        default=DEFAULT_12P_NODE_HOSTS[0],
        type=str,
        dest="i2p_host",
        help="Specify the location of the i2p node you want to connect to peers on i2p with"
    )

    proxy_param_grp.add_argument("--i2p-port",
        default=DEFAULT_I2P_PORTS[0],
        type=port,
        dest="i2p_port",
        help="Specify the port of the i2p node to connect to peers on i2p with"
    )

    region_grp = aparser.add_argument_group(title="Peer Node Region Control", description="""
        Provides control of the regions to pull nodes from.

        By default, if unspecified, this will try to pull nodes by using your timezone
        to get a rough location (continent level, in most cases) - it is recommended 
        you specify a region if possible because it makes the network more efficient
        and means you are unlikely to get significant amounts of packets routed through
        your own node.

        The automatic region selector expects large scale regions of the form /asia, /oceania
        /europe, /africa-{east west}, and /america-{north south} - we also accept /north-america 
        and /south-america for compatibility purposes but if you can avoid it I recommend doing so.

        and it can also find nodes
        specifically in /europe/united-kingdom and /europe/uk and /europe/gb and 
        /europe/ireland and /europe/spain and /europe/portugal and /europe/france. The autoselector will fall back on /europe for these if none are
        present, and will generally fall back on the next level up if there are insufficient
        nodes when automatic node location is enabled.

        The prefix nature of the regions means that regions of subcontinents should be as 
        dashes AFTER the main continent name so, say, /africa includes west and east africa.
    """)

    region_grp.set_defaults(regions=[])

    region_grp.add_argument("-r", "--region",
        type=str,
        action="append",
        dest="regions",
        help="""Specify a region to pull nodes from. This option can be repeated. 
        This option also has no effect if your initial peer list location is a file.
        Regions, after all, are just sets of files.
        Regions are of the form /<region>/.../<subregion>, such that / effectively
        selects all regions in the location. Region names are effectively relative 
        directory paths (though they start with /). They are checked for not moving out
        of the directory e.g. via "..". 
        """
    )

    region_grp.add_argument("-a", "--auto-region",
        action="append_const",
        dest="regions",
        help="""
        If we cannot pull sufficient nodes from the specified region, then try to 
        extract your continent/ish-level rough location via timezone. If region is left
        unspecified, this is used automatically. When we cannot find enough nodes 
        satisfying the filters, we go up a network level and repeat until root.

        When you specify regions manually, you can ALSO specify this option. When you
        do, if there are not enough peers in the regions you specified matching the 
        filter, the script will then engage in automatic peer selection in the hopes of
        finding more.
        """,
        const = REGION_DETECT.AUTOREGION
    )

    region_grp.add_argument("--peer-select-method",
        action="store",
        default=REGION_SELECT.UNIFORM_BY_REGION,
        dest="region_selection_method",
        type=REGION_SELECT.ap_type,
        choices=REGION_SELECT.ap_choices,
        help="""
        This allows you to select how non-automatic (i.e. commandline passed) regions have nodes selected from them.
        
        UNIFORM_BY_PEER means that the peer is uniformly (randomly) chosen from the overall set of peers of all the regions.

        UNIFORM_BY_REGION means that a region is uniformly (randomly) chosen from the set of regions, then a peer is uniformly chosen
        from within that region. This is good for ensuring that you do not end up with all your nodes in one region because there are
        way more nodes in said region, and is the default.

        PRIORITY_BY_REGION means that peers are chosen randomly solely from the first passed region, and the next regions (in order) only
        get accessed if there are no more nodes satisfying the specified filters in said region.

        PRIORITY_BY_DEPTH means that regions are first sorted in terms of their depth (i.e. / is depth 0, /europe is depth 1, /europe/uk is 
        depth 2, etc.). Then you select in the manner of UNIFORM_BY REGION only within the deepest group, until it runs out of nodes, then you
        move up to the next depth level, and again. This is roughly what automatic region finding does.
        """
    )

    tcp_tls_addr_filter_grp = aparser.add_argument_group(title="Peer Filters", description="""
    Set filters for the types of peer addresses you desire to use (the filters apply
    to a given set of the addresses). By default we only add IPv4 peers since most
    people have IPv4 internet.
    """)

    tcp_tls_addr_filter_grp.set_defaults(peer_address_filters=[])
    tcp_tls_addr_filter_grp.add_argument("--ipv4",
        action="append_const",
        const=ipv4,
        dest="peer_address_filters",
        help="""Include IPv4 addresses in the list of peer nodes. Applies to TLS and 
        TCP peers."""
    )

    tcp_tls_addr_filter_grp.add_argument("--ipv6",
        action="append_const",
        const=ipv6,
        dest="peer_address_filters",
        help="""Include IPv6 addresses in the list of peer nodes. Applies to TLS and
        TCP peers"""
    )

    tcp_tls_addr_filter_grp.add_argument("--dns-addr",
        action="append_const",
        const=dns,
        dest="peer_address_filters",
        help="""Include addresses other than IPv4 and IPv6 addresses. This is used
        to pull DNS addresses into the node list.
        """
    )

    modegrp = aparser.add_mutually_exclusive_group()
    modegrp.set_defaults(program_mode=PROGRAM_MODE.MODIFY_CONFIG)
    
    modegrp.add_argument("--list-regions",
        action="store_const",
        const=PROGRAM_MODE.LIST_REGIONS,
        dest="program_mode",
        help="""Tell the program to list the available regions without writing the config. 
        This does nothing with the location mode being a file"""
    )

    modegrp.add_argument("--list-peers",
        const=PROGRAM_MODE.LIST_SELECTED_PEERSET,
        action="store_const",
        dest="program_mode",
        help="""Tell the program to list the peers that it has selected to choose from 
        without writing them. It prints 1 peer on each line."""
    )

    modegrp.add_argument("--list-peers-no-localise",
        action="store_const",
        dest="program_mode",
        const=PROGRAM_MODE.LIST_SELECTED_PEERSET_NO_LOCALISE,
        help="""
        Same as list peers mode, but tell the program not to localise peer addresses to 
        your tor/i2p host/port setup, instead replacing those with "<tor-host>", 
        "<tor-port>", "<i2p-host>" and "<i2p-port>" in the specifier.
        """
    )

    modegrp.add_argument("--modify-config",
        const=PROGRAM_MODE.MODIFY_CONFIG,
        action="store_const",
        dest="program_mode",
        help="""Tell the program to do the default behaviour of actually writing the
        config"""
    )

    peercountgroup = aparser.add_argument_group(title="Peer Count Group", description="""
        Commands for selecting the number of peers to actually pull into the config file.
    """)
    mutexpcgrp = peercountgroup.add_mutually_exclusive_group()
    mutexpcgrp.set_defaults(max_peers=4)
    mutexpcgrp.add_argument("-m", "--max", 
        type=int,
        dest="max_peers",
        help="""
        Tell the program how many peers to try and select. Default is 4 and it is not
        recommended to change this.
        """
    )

    mutexpcgrp.add_argument("--no-max",
        action="store_const",
        dest="max_peers",
        const=math.inf,
        help="""
        Tell the program that it should configure yggdrasil to connect to all the
        peers it can within it's filter set. This is HEAVILY NOT RECOMMENDED AT ALL.
        """
    )

    if len(args) > 0 and args[0] == "--work":
        aparser.add_argument("--work",
            action="store_const",
            dest="work",
            const=True,
            default=True,
            help="""
            Marks this program as a worker rather than something that deescalates
            privilege if possible

            This is an internal argument and not intended for users.
            """
        )
        aparser.set_defaults(config_override_array=[])
        aparser.add_argument("--overwrite-config",
            action="append",
            dest="config_override_array",
            type=str,
            help="""
            Add another config file to the list in order of config files to override.

            This is an internal argument and not intended for users.
            """
        )

        aparser.add_argument("--drop-to-uid",
            action="store",
            type=int,
            dest="drop_uid",
            default=None,
            help="""
            Specify a uid to drop privileges to.
            """
        )

        aparser.add_argument("--drop-to-gid",
            action="store",
            type=int,
            dest="drop_gid",
            default=None,
            help="""
            Specify a gid to drop privileges to,
            """
        )

    # Protocol list
    res = aparser.parse_args(args)
    if len(res.peer_protocols) == 0:  # Option is unspecified so protocols are empty.
        res.peer_protocols = [PEER_MODE.TCP, PEER_MODE.TLS]
    res.peer_protocols = set(res.peer_protocols)  # Make protocols unique ^.^
    
    # By default only include ipv4 peers since most people only have ipv4 internet
    # connections :(
    if len(res.peer_address_filters) == 0:
        res.peer_address_filters = [ipv4]

    # Move autoregion to the end so it gets lowest priority and ensure there is
    # only one.
    if REGION_DETECT.AUTOREGION in res.regions:
        while REGION_DETECT.AUTOREGION in res.regions:
            res.regions.remove(REGION_DETECT.AUTOREGION)
        res.regions.append(REGION_DETECT.AUTOREGION)

    if len(res.regions) == 0:
        res.regions = [REGION_DETECT.AUTOREGION]

    return res








################################################################################
########## REGION TREE / REGION DATA CONVERSION AND MANIPULATION ############ <3
################################################################################


def get_region_data_from_region_tree(
        region_tree: Dict, 
        data_generator: Callable[[Region], T]=lambda x: None
    ) -> OrderedRegionDataCollection[T]:
    """
    Get a list of paths accessed in order of tree depth, up to /, for the region tree
    as produced by build_region_tree.

    Returns an ordered list of (path, path depth, None[or data_generator(path)]) where it is ordered in 
    (top down) path depth.

    This essentially turns a tree of regions into an ordered region list with no data (though you can for sure
    add data).
    """

    # First pull a list of all paths in a pair (path, depth, ) from the tree.
    paths = []
    stack = [(region_tree, Region('/'))]  # List of to-be-processed (node, our_path), and we pull off
    # this, generate path, and add any subnode to the stack, then repeat until
    # empty.
    while len(stack) > 0:
        curr_node, our_path = stack.pop()
        # /europe has depth 1, /europe/gang has depth 2, etc.
        depth = region_depth(our_path)
        
        for region, subnode in curr_node.items():
            stack.append((subnode, join_regions(our_path, region)))
       
        paths.append((our_path, depth, data_generator(our_path)))

    # Now sort by decreasing depth
    paths.sort(lambda path: (path[1], len(path[0])), reverse=False)
    return paths


def get_region_tree_from_region_data(_regions: OrderedRegionDataCollection[T]) -> Dict:
    """
    Turn an ordered region collection into a region tree as produced by
    build_region_tree
    """
    region_list_accessor = (Region(region) for region, depth, data in _regions)
    return build_region_tree(region_list_accessor)


def build_region_tree(_regionlist: Iterable[Region]) -> Dict:
    """
    Build a tree of regions from a list of regions. Each node is another dictionary
    holding all subregion nodes (if empty then we are a leaf)

    Note that this DOES NOT account for prefixes and is more of a method of
    finding supernodes for a set of paths. i.e. you shove all your registered valid 
    prefixes in here and find all matching real nodes with each prefix.
    """
    regionlist = (normalise_region(r) for r in _regionlist)
    root = {}
    for r in regionlist:
        path = r.split('/')[1:]  # Always have / at start so useless prepended ''
        # presence.
        local_root = root  # The current node in the tree we are at running through
        # the path.
        if path[1] == '':  # The path is root path.
            continue  # We already have root path
        for element_idx in range(len(path)):
            if path[element_idx] not in local_root.keys():
                local_root[path[element_idx]] = {}
            local_root = local_root[path[element_idx]]

    return root

def get_all_containing_regions(_regions: Iterable[Region]) -> OrderedRegionDataCollection[None]:
    """
    Get every region required to contain the given list of regions, as an ordered
    collection of regions, as well as all the parents, of course ordered by depth ^.^
    """
    tree = build_region_tree(_regions)
    return get_region_data_from_region_tree(tree)







######################################################################################
######################## REGION PEER FILTER AND SELECTION ############################
######################################################################################


class DataExtractorState(Generic[T], dict):
    """
    Just a simple class to specify that this is a data extractor state.
    """


PeerExtractorState = DataExtractorState[GenPeerAddr]

class _RegionalDataExtractor_Fn(Protocol[T]): 
    """
    Protocol for regional extractor functions.
    """
    def __call__(self, 
        already_extracted_data: Set[T],
        region_list: List[Region],
        region_data_set: OrderedRegionDataCollection[Collection[T]],
        state: Optional[DataExtractorState[T]]=None
    ) -> Tuple[
        Optional[T], 
        Optional[DataExtractorState[T]]
    ]:
        pass



class RegionalDataExtractor(Generic[T]):
    """
    Holds a method of extracting data from an ordered list of regions.

    The arguments passed are a set of the already extracted data, an ordered list of all the 
    regions to pull from, and the ordered region data holding the data of available regions 
    from the files or otherwise.

    It should return (None, _) if there is no more data it can pull, else it should return a
    new data peice, not in the set of existing selected data, selected from the data.

    The second return component is an optional state parameter which is allowed to be used to 
    accelerate repeated searches. The function should be able to work if None is passed 
    as the state. 

    If state is passed, then you are allowed to assume that List[Region] has
    not changed since you returned the state beforehand - and that the data list has not changed - 
    and that the only difference in already_extracted_data is that the returned data
    "last call" was added to it if the resulting data was not None.

    This class is designed to be used as a method tag/@/whatever thing.
    """
    def __init__(self, data_extractor: _RegionalDataExtractor_Fn):
        self._extractor = data_extractor

    def do_extraction(self, 
        already_extracted_data: Set[T],
        region_list: List[Region],
        region_data_set: OrderedRegionDataCollection[Collection[T]],
        state: Optional[DataExtractorState[T]]=None
    ) -> Tuple[Optional[T], Optional[DataExtractorState[T]]]:
        """
        Do the actual extraction
        """
        return self._extractor(already_extracted_data, region_list, region_data_set, state)


# Since the methods for peer extraction apply just as well for any iterable data,
# we provide more general solution.
RegionalPeerExtractor = RegionalDataExtractor[GenPeerAddr] 

def filter_peers_by_type( 
        peers: Iterable[T], 
        allowed_modes: Set[PEER_MODE],
        fil: Filter,
        peer_extractor: Callable[[T], GenPeerAddr]
    ) -> Iterable[T]:
    """
    Filter the peer collection to only include peers of the given mode
    that satisfy the given filter.
    """
    return filter(lambda elem: (peer_extractor(elem)[0] in allowed_modes and fil(elem)), peers)


@RegionalDataExtractor
def ps_uniform_by_peer(
        already_extracted_data: Set[T],
        regions: List[Region],
        region_datas: OrderedRegionDataCollection[Collection[T]],
        state: Optional[DataExtractorState]
    ) -> Tuple[Optional[T], DataExtractorState]:
    """
    Select in the manner specified for REGION_SELECT.UNIFORM_BY_PEER
    """
    state = by_default(state, DataExtractorState())
    if len(state) == 0:
        # Peers not yet extracted, and cache the region depths.
        state.depths = get_depths(region_datas)
        state.unpulled_data: Set[T] = get_all_broadest_contained_region_data(regions, region_datas, depths=state.depths)
        state.unpulled_data.difference_update(already_extracted_data)

    if len(state.unpulled_data) == 0:
        return (None, state)
    else:
        new_data: T = random.choice(list(state.unpulled_data))
        # discard does not check existence.
        state.unpulled_data.discard(new_data)
        return new_data, state

@RegionalDataExtractor
def ps_uniform_by_region(
        already_extracted_data: Set[T],
        regions: List[Region],
        region_datas: OrderedRegionDataCollection[Collection[T]],
        state: Optional[DataExtractorState]
    ) -> Tuple[Optional[T], DataExtractorState]:
    """
    Select peers in the method specified by REGION_SELECT.UNIFORM_BY_REGION

    In particular we find the largest contained regions for each area.
    """
    state = by_default(state, DataExtractorState())
    if len(state) == 0:
        # Regions 
        state.depths = get_depths(region_datas)
        # Locate the largest regions contained by each selected region, to pull peers from.
        # Exact regions will naturally result in themselves. Empty/nonexistent regions will of course
        # not result in anything at all ;-;
        state.unpulled_regions: Dict[Region, Set[T]] = {}
        for _region in regions:
            region_data: Set[T] = get_all_broadest_contained_region_data([_region], region_datas, state.depths)
            
            # Remove all preselected peers.
            region_data.difference_update(already_extracted_data)

            # Update the megalist of peers.
            state.unpulled_regions[_region] = region_data

    # We know that, based on the assumptions of a RegionalDataExtractor, that our state holds exactly the
    # unselected data available for each region. We will remove all empty sets each time.
    to_del_regions = set()
    for region, unselected_region_data in state.unpulled_regions.items():
        if len(unselected_region_data) == 0:
            to_del_regions.add(region)
    for r in to_del_regions:
        del state.unpulled_regions[r]
    
    # Check we have peer/data
    if len(state.unpulled_regions) == 0:
        return None, state

    # Pick the actual peer/data
    non_empty_region_pick = random.choice(list(state.unpulled_regions.keys()))
    new_data = random.choice(list(state.unpulled_regions[non_empty_region_pick]))
    
    # Remove it from everything.
    for _, dataset in state.unpulled_regions.items():
        dataset.discard(new_data)

    return new_data, state


@RegionalDataExtractor
def ps_priority_by_region(
        already_extracted_data: Set[T],
        regions: List[Region],
        region_datas: OrderedRegionDataCollection[Collection[T]],
        state: Optional[DataExtractorState]
    ) -> Tuple[Optional[T], DataExtractorState]:
    """
    Select data first from the first region until they run out,
    until the second/third/etc. region until everything is run out.
    """
    state = by_default(state, DataExtractorState())
    if len(state) == 0:
        state.depths = get_depths(region_datas)

        state.current_region_idx: int = 0
        # This is the peers available for each region. We keep this
        # updated.
        state.available_region_data: List[Set[T]] = []
        # First pull them...
        for idx in range(len(regions)):
            contained_data = get_all_broadest_contained_region_data([regions[idx]], region_datas, state.depths)
            contained_data.symmetric_difference_update(already_extracted_data)
            state.available_region_data.append(contained_data)

    # Locate the first non-empty region. If not found, then there are no data chunks available
    idx = state.current_region_idx
    while idx < len(regions) and len(state.available_region_data[idx]) == 0:
        idx += 1
    state.current_region_idx = idx

    if state.current_region_idx == len(regions):
        return (None, state)

    # Randomly pick a data chunk.
    data_chunk = random.choice(list(state.available_region_data[state.current_region_idx]))
    # Remove from the available region data for the region
    state.available_region_data[state.current_region_idx].discard(data_chunk)

    return data_chunk, state

@RegionalDataExtractor
def ps_priority_by_depth(
        already_extracted_data: Set[T],
        regions: List[Region],
        region_datas: OrderedRegionDataCollection[Collection[T]],
        state: Optional[DataExtractorState]
    ) -> Tuple[Optional[T], DataExtractorState]:
    """
    Select peers/data according to that specified by REGION_SELECT.PRIORITY_BY_DEPTH 
    """
    state = by_default(state, DataExtractorState())
    if len(state) == 0:
        state.depths = get_depths(region_datas)

        # Now we first want to generate the set of regions by depth.
        region_depth_map: Dict[int, List[Region]] = dict()

        for region in regions:
            region_depth_map.setdefault(region_depth(region), []).append(region)

        state.dataset_by_depth: Dict[int, Set[T]] = dict()  # We maintain this to only contain
        # data not in already_extracted_data

        for depth, regionlist in region_depth_map.items():
            # Pull out all data for the given region list - which is of the given depth -
            # and store that in our by-depth dataset.
            our_dataset = get_all_broadest_contained_region_data(regionlist, region_datas, state.depths)
            our_dataset.difference_update(already_extracted_data)
            state.dataset_by_depth[depth] = our_dataset
        
        # Get the max depth and set it as the current.
        state.current_depth = max(state.dataset_by_depth.keys())
    
    # Find the next smallest non-empty and existent depth to scan through...
    d = state.current_depth
    while ((d not in state.dataset_by_depth) or (len(state.dataset_by_depth[d]) == 0)) and d > -1:
        d -= 1
    state.current_depth = d

    # If we are at -1, we are OUT of data!
    if state.current_depth == -1:
        return (None, state)

    # Else choose uniformly.
    new_data = random.choice(list(state.dataset_by_depth[state.current_depth]))
    # and remove from each part of the datasets! nyaaaa!
    for _, depthset in state.dataset_by_depth.items():
        depthset.discard(new_data)
    
    return (new_data, state)


def select_data(
        regions: List[Region],
        selection_method: RegionalDataExtractor[T],
        region_data: OrderedRegionDataCollection[Collection[T]],
        max_data_count: ComparableWith[int],
        inplace_set_for_result: Optional[Set[T]]=None
    ) -> Set[T]:
    """
    Try to extract at most max_data_count from the region data!.

    Returns the set. Note that if you already have a set with some already selected
    things you can add that as an argument and it will be modified in place as well
    as returned.
    """
    state = None
    result = by_default(inplace_set_for_result, set())

    while max_data_count > len(result):
        curr_data, state = selection_method.do_extraction(
                result,
                regions,
                region_data,
                state
            )
        if curr_data is None:
            break
        result.add(curr_data)

    return result

def select_peers( 
        regions: List[Region],
        peer_selection_method: RegionalPeerExtractor,
        peer_data: OrderedRegionedPeerCollection,
        max_peer_count: ComparableWith[int],
        inplace_set_for_result: Optional[Set[GenPeerAddr]]=None
    ) -> Set[GenPeerAddr]:
    """
    Select peers from peer_data, using the given list of regions, and the peer
    selection method given.

    It attempts to pull a maximum of max_peer_count from the peer data.
    """
    return select_data(
            regions, 
            peer_selection_method, 
            peer_data, 
            max_peer_count, 
            inplace_set_for_result
        )

peer_selection_method_map: Dict[REGION_SELECT, RegionalPeerExtractor] = {
    REGION_SELECT.UNIFORM_BY_PEER: ps_uniform_by_peer,
    REGION_SELECT.UNIFORM_BY_REGION: ps_uniform_by_region,
    REGION_SELECT.PRIORITY_BY_DEPTH: ps_priority_by_depth,
    REGION_SELECT.PRIORITY_BY_REGION: ps_priority_by_region
}










################################################################################
################ FILE/DIRECTORY/REGION INTERACTION #############################
################################################################################



def generate_region_files_from_filepaths(
        filepaths: List[FilePath], 
        remove_dot_suffix:bool=True
    ) -> Dict[Region, List[FilePath]]:
    """
    Turn a list of filepaths into a list of regions pointing to all common sets
    of files for the given prefix. 

    This strips off dots if we say so. If you treat directories as subfiles, then you can get
    some paths out of this that effectively can be turned into subregion paths for
    the respective directories.

    Note that this ignores dotfiles, which is good.

    This also does not in any way interact with the file system. In particular, 
    this is actually very good for usage with relative paths to get the endpoints!.


    >>> generate_region_files_from_filepaths([
    ...  "europe/united-kingdom.md",
    ...  "/europe/united-kingdom.txt",
    ...  "/europe-west.md"
    ... ])
    {
        "/europe/united-kingdom": ["/europe/united-kingdom.md", "europe/united-kingdom.txt"], 
        "/": ["/europe-west.md"]
    }

    """
    r = {}
    for fname in filepaths:
        fname_head, fname_tail = os.path.split(fname)
        fname_region_suffix = fname_tail
        if remove_dot_suffix:
            # Always produces at least one.
            fname_dotsplit = fname_region_suffix.split('.')[0]
            # If the result is empty then we have a dotfile since there is nothing
            # before the dot.
            # which we account for by not adding it.
            # since this is empty.
            fname_region_suffix = fname_dotsplit
        if not fname_region_suffix.startswith('.') and not len(fname_region_suffix) == 0:
            # Then add us.
            region_name = os.path.join(fname_head, fname_region_suffix)
            region_name = normalise_region(region_name)
            r.setdefault(region_name, []).append(fname)

    return r


def get_available_regions_in_directory(base_dir: str) -> Dict[Region, List[FilePath]]:
    """
    Get all available regions for the given base directory - NOT confined to temp dir.

    This returns a map of (possible region -> [all files, where for files we strip all 
    past the first dot when calculating the regions to add to])

    This automatically excludes regions as specified in the exclude region function.
    """
    # We want to run os.walk as BOTTOMUP so we can build up lists of files for all the
    # subregions before going for the regions.
    topdir = base_dir

    region_map = {}
    dotfile_regions = set()

    for abs_directory_path, subdir_names, filenames in os.walk(topdir, topdown=False):
        # Bottom up means we can just pull all data from the previous matching regions
        # of each directory into this directory, as well as add filepaths.

        # Make our directory regions and current directory path relative to top.
        # And also normalise to get rid of weird dots appearing ;-;
        directory_path = os.path.normpath(os.path.relpath(abs_directory_path, topdir))

        # Note there's some weirdness here - in particular, when AT the top
        # directory it returns /. instead of / (which makes sense in most cases) because
        # it's a relative path.
        # however when we want to make region paths we need just /

        # Pull out files from subdirectory regions.
        # making sure to remove any subdir names from the map if they are dotfiles.
        # at the end
        dotfile_subdir_names = set(k for k in subdir_names if k.startswith('.'))

        # Regions which we will remove later because they are dotfiles.
        # along with all subregions.
        dotfile_regions.update(
            join_regions(Region(directory_path), Region(sd)) 
            for sd in dotfile_subdir_names
        )
        
        # These are directory-based subregions of this region.
        directory_regions = [
            join_regions(Region(directory_path), Region(sd)) 
            for sd in subdir_names
        ]

        files = []

        # This is taking the files from the directory-based subregions and adding
        # them to our own.
        # We also select out dotfile/dot-directories to avoid adding those 
        # files (which will be removed later).
        for dirregion in directory_regions:
            if (dirregion not in dotfile_regions) and not exclude_region(dirregion):
                files.extend(region_map[dirregion])
        

        # Now create the subregions appropriately for each file in this directory,
        # excluding dotfiles ^.^ - also postfilters for being in exclude region type.
        fregions = generate_region_files_from_filepaths([
            os.path.join(directory_path, fn) 
            for fn in filenames if not fn.startswith(".")
        ])

        
        # Add those file-based regions to ourselves, unless we should exclude...
        for file_region, region_files in fregions.items():
            if not exclude_region(file_region):
                region_map[file_region] = region_files
                files.extend(region_files)

        # Then we want to create a region for this directory using the files
        # we've just built up.
        our_region = normalise_region(Region(directory_path))
        if our_region == "/.":
            our_region = "/"
        region_map[our_region] = files

    # Now we rip apart all the dotfile child regions.
    def dotregion_remove(region: Region) -> bool:
        return any(
                is_subregion(region, dotregion) 
                for dotregion in dotfile_regions
            ) or exclude_region(region)

    # can't inline remove dict elements
    to_remove = set()
    for region, files in region_map.items():
        if dotregion_remove(region):
            to_remove.add(region)
    for a in to_remove:
        del region_map[a]
 
    return region_map


def get_peers(base_directory: FilePath, filenames: List[FilePath]) -> Set[GenPeerAddr]:
    """
    Get the generalised peers from a given set of filenames with a base directory
    specified. The filenames/paths other than base directory are NORMALISED.

    Note that you should still be careful of symbolic links, but this method 
    tries to prevent symbolic link attacks by using os.O_NOFOLLOW
    """
    result = set()
    for fname in filenames:
        with open(
                os.path.join(
                    base_directory, 
                    os.path.normpath(fname)
                ), 
                'rt', opener=nosymlinks_fileopener
            ) as f:
            raw_data = f.read()
            genpeers = scan_for_peer_uris(raw_data)
            result.update(genpeers)
    return result
    









##############################################################################
################### PROGRAM EXECUTION ########################################
##############################################################################

def conditionally_genconf_config_file(config_file):
    """
    Check if the given config file exists. If not, 
    then use yggdrasil -genconf to create it.
    """
    if not os.path.exists(config_file):
        print("No config file at location, generating...", file=sys.stderr)
        # If there is nothing there then generate the config file
        subprocess.run(
            "yggdrasil -genconf > " + shlex.quote(config_file), shell=True
        )


class Context(object):
    """
    Holds the actual program context :)

    this is a "withable" thing that essentially works as with the
    temp dir.
    """
    def __init__(self, raw_sysargv: Optional[List[str]], existing_pargs=None):
        """
        Create a context, along with a temporary directory to use ^.^ 

        Also allows running with an existing set of arguments. If there is not, 
        then it is expected to have raw sys.argv provided.
        """
        self.temporary_directory_obj = None  # context/with Object
        self.temporary_directory = None  # string pointing to actual directory.

        # Simple -h appender if no args provided.
        if existing_pargs is None:
            a = [v for v in raw_sysargv]
            if len(raw_sysargv) == 1:
                a.append("-h")
            self.parsed_args = parse_args(a[1:])
        else:
            self.parsed_args = existing_pargs

        self.nc = self.parsed_args.noconfirm

        self.peer_prefix_format_kwargs: Dict[str, str] = {
            "torhost": self.parsed_args.tor_host,
            "torport": self.parsed_args.tor_port,
            "i2phost": self.parsed_args.i2p_host,
            "i2pport": self.parsed_args.i2p_port
        }

        self.allowed_peer_modes: Set[PEER_MODE] = set(self.parsed_args.peer_protocols) 
        self.peer_address_filters: Set[Filter] = set(self.parsed_args.peer_address_filters)
        # as a backup.

        self.peer_select_method: REGION_SELECT = self.parsed_args.region_selection_method

        # Pull out if we are doing auto or not. We know from the argparsing function 
        # that if there is an auto selector it is at the end, and we also know there
        # is at least one element.

        if self.parsed_args.regions[-1:][0] == REGION_DETECT.AUTOREGION:
            self.auto_region_find: bool = True
            self.regions: List[Region] = [normalise_region(r, True) for r in self.parsed_args.regions[:-1]]
        else:
            self.auto_region_find: bool = False
            self.regions: List[Region] = [normalise_region(r, True) for r in self.parsed_args.regions]
            
        self.max_peers: float = self.parsed_args.max_peers  # Can be inf


    def safe_abs_path(self, path_from_tdir: str) -> str:
        """
        Get a hopefully safe (normalised) absolute path, where path_from_tdir is a relative 
        path (though it may be prefixed with /) from the temporary directory.
        """
        nrp = os.path.normpath(path_from_tdir)
        return os.path.join(self.temporary_directory, nrp)

    def pull_git_repository(self, git_repo: str, dirname: str="repo", mute_output: bool=False):
        """
        This function attempts to pull in an arbitrary git repo into 
        a subdirectory of the temporary directory, named "repo" by default.

        It also goes into the directory and removes the .git directory.

        Throws a CalledProcessError if there was an error.

        You can also mute the output of git (i.e. pipe it's output to nowhere instead of stdout)
        """
        extra_kwargs = {}
        if mute_output:
            extra_kwargs["stdout"] = subprocess.DEVNULL
            # git uses stderr to vomit a lot of stuff, so dump into devnull
            extra_kwargs["stderr"] = subprocess.DEVNULL

        res = subprocess.run(["git", "clone", git_repo, self.safe_abs_path(dirname)], check=True, **extra_kwargs)
        res.check_returncode()

        unable_to_remove_paths = []
        def onerror(function, path, exception_info):
            unable_to_remove_paths.append(path)

        shutil.rmtree(self.safe_abs_path(os.path.join(dirname, ".git")), onerror=onerror)
        if len(unable_to_remove_paths) > 0:
            raise subprocess.CalledProcessError(1, shlex.join(["rm", "-rf", unable_to_remove_paths[0]]))

    def localise_all_peer_uris(self, generalised_peer_addresses: Iterable[GenPeerAddr]) -> Iterable[str]:
        """
        Localise all the generalised peer addresses ^.^
        """
        return (localise_peer_uri(p, **self.peer_prefix_format_kwargs) for p in generalised_peer_addresses)

    def __enter__(self):
        self.temporary_directory_obj = tempfile.TemporaryDirectory()

        self.temporary_directory = self.temporary_directory_obj.__enter__()

        return self
    
    def __exit__(self, *args, **kwargs):
        tdir = self.temporary_directory_obj

        # Reset both the string and the object
        self.temporary_directory_obj = None
        self.temporary_directory = None

        return tdir.__exit__(*args, **kwargs)


    def preamble_get_files(self, mute: bool=False):
        """
        A preamble that, if we are in a directory or git mode, grabs all
        the files, and puts the directory they are in as self.peerlist_directory

        If we are in filemode, we just set the .peerlist_directory to the peerlist 
        location (tho that isn't actually a directory)
        """
        # When we are in file or git mode, make this.
        default_peerlist_rdirname = "peerlist"
        default_peerlist_directory = self.safe_abs_path(default_peerlist_rdirname)

        if self.parsed_args.peerlist_location_type == PEER_LIST_LOCATION_MODE.FILE:
            os.makedirs(default_peerlist_directory)
            self.peerlist_directory = default_peerlist_directory
        elif self.parsed_args.peerlist_location_type == PEER_LIST_LOCATION_MODE.GIT:
            if confirm("Clone git repository '{}'".format(self.parsed_args.peerlist_location), mute, default=True):
                self.pull_git_repository(self.parsed_args.peerlist_location, default_peerlist_rdirname, mute_output=mute)
                self.peerlist_directory = default_peerlist_directory
        elif self.parsed_args.peerlist_location_type == PEER_LIST_LOCATION_MODE.DIRECTORY:
            self.peerlist_directory = self.parsed_args.peerlist_location

    def preamble_get_all_peers(self, mute: bool=False):
        """
        This gets hold of the generic peer addresses for every region.

        Note that this does call the get files preamble, so you can assume that
        any state it sets will also be set by calling this preamble.

        This either sets self.peer_collection to a List
        (which is an OrderedRegionedPeerCollection) - if we are in git and directory mode
        - or to a pure Set[GenPeerAddr] if we are in file mode.

        This also prefilters the peers (constructs the filters).
        """
        self.preamble_get_files(mute=mute)

        if self.parsed_args.peerlist_location_type in {PEER_LIST_LOCATION_MODE.DIRECTORY, PEER_LIST_LOCATION_MODE.GIT}:
            # peerlist directory is actually a directory containing the peer list :p
            regions = get_available_regions_in_directory(self.peerlist_directory)
            ordered_regions, slashcounts = order_region_data(regions)
            # Now, we pull the regions out.
            def transform(fnames: List[FilePath]) -> Set[GenPeerAddr]:
                """
                Turn a file into a list of peers
                """
                return get_peers(self.peerlist_directory, fnames)

            self.peer_collection = parse_region_data(ordered_regions, transform)

            #print(self.peer_collection)

            def filtering_transform(peerset: Set[GenPeerAddr]) -> Set[GenPeerAddr]:
                """
                Transform to filter the peers according to criteria.
                """
                return set(filter_peers_by_type(
                    peerset,
                    self.allowed_peer_modes,
                    or_filters(*self.peer_address_filters),
                    lambda x: x
                ))
            self.peer_collection = parse_region_data(self.peer_collection, filtering_transform)


        elif self.parsed_args.peerlist_location_type == PEER_LIST_LOCATION_MODE.FILE:
            # All we have to do is load the file and scan, lol.
            with open(self.parsed_args.peerlist_location, "rt") as our_file:
                data = our_file.read()
                self.peer_collection = set(scan_for_peer_uris(data))

            # Now we filter peers!
            self.peer_collection = set(filter_peers_by_type(
                self.peer_collection, 
                self.allowed_peer_modes, 
                or_filters(*self.peer_address_filters),
                lambda x: x
            ))

    def preamble_select_peers(self, mute: bool=False):
        """
        Select a set of (un-setup-localised) peers using the filters and methods chosen. 

        It sets them to self.selected_peers, as a set, in all cases.
        """
        self.preamble_get_all_peers(mute=mute)
        # Now we have to select peers appropriately from peer_collection.
        selected_peers: Set[GenPeerAddr] = set()

        #print(self.peer_collection)
        
        # For file mode there is no way to select but random uniform choice....
        if self.parsed_args.peerlist_location_type == PEER_LIST_LOCATION_MODE.FILE:
            remaining_peers = set(self.peer_collection)
            while len(selected_peers) < self.max_peers and len(remaining_peers) > 0:
                next_data = random.choice(list(remaining_peers))
                selected_peers.add(next_data)
                remaining_peers.discard(next_data)

        # Else, time to do  w e i r d a u t o s h i t
        elif self.parsed_args.peerlist_location_type in {
                PEER_LIST_LOCATION_MODE.DIRECTORY, 
                PEER_LIST_LOCATION_MODE.GIT
            }:
            # First, try nonauto if region list is not empty.
            if len(self.regions) != 0:
                selected_peers = select_peers(
                        self.regions, 
                        peer_selection_method_map[self.peer_select_method], 
                        self.peer_collection, 
                        self.max_peers, 
                        selected_peers
                )
            # If we don't get enough, try auto! with our timezome
            if len(selected_peers) < self.max_peers and self.auto_region_find and confirm(
                    "Only got {} peers, when you specified {} peers. Attempt auto-region-selection with timezone".format(
                        len(selected_peers), 
                        self.max_peers
                    ),
                    mute,
                    default=True,
                    noconfirm_default=self.auto_region_find
                ):
                # Pull the timezone ^.^ (not a function for some reason)
                tz = time.timezone
                autopeer_list = generate_timezone_region_list(tz) 

                # Then generate "fallback region scanner list of regions"
                region_paths = get_containing_regions_and_data(autopeer_list, self.peer_collection)
                regions = [region for region, _, _ in region_paths]
                selected_peers = select_peers(
                    regions, 
                    ps_priority_by_depth, 
                    self.peer_collection, 
                    self.max_peers, 
                    selected_peers
                )
        
        self.selected_peers = selected_peers


    # Here we switch to the combine-y stuff ^.^
    def mode_list_regions(self):
        self.preamble_get_files(mute=True)
    
        regions = get_available_regions_in_directory(self.peerlist_directory)
        ordered_regions, depths = order_region_data(regions)
        
        # Get the broadest regions contained by user selected regions.
        selected_regions = get_broadest_contained_regions(
            self.regions, 
            ordered_regions,
            depths
        )
        
        for k in selected_regions:
            print(k)


    def mode_list_peerset(self, do_localise: bool): 
        self.preamble_select_peers(True)

        no_localise_peer_format_kwargs = {
            "torhost": "<tor-host>",
            "torport": "<tor-port>",
            "i2phost": "<i2p-host>",
            "i2pport": "<i2p-port>"
        }

        peerlist = None
        if do_localise:
            peerlist = [localise_peer_uri(p, **self.peer_prefix_format_kwargs) for p in self.selected_peers]
        else:
            peerlist = [localise_peer_uri(p, **no_localise_peer_format_kwargs) for p in self.selected_peers]
        for p in peerlist:
            print(p)

    def mode_modify_config(self):
        """
        The mode for modifying configs!
        """
        # Noconfirm setting passed on.
        self.preamble_select_peers(self.nc)
        peerlist = [localise_peer_uri(p, **self.peer_prefix_format_kwargs) for p in self.selected_peers]
        
        #subprocess.run(["ls", "-lAhR", self.temporary_directory])
        conditionally_genconf_config_file(self.parsed_args.config_file)

        # Generates the normalised config file in json for us to manipulate
        # yggdrasil uses stderr
        result = subprocess.run([
            "yggdrasil", 
            "-json", 
            "-normaliseconf", 
            "-useconffile", 
            os.path.abspath(self.parsed_args.config_file)
        ], capture_output=True)
        
        the_json_config = str(result.stdout, encoding="utf8")
        the_json = json.loads(the_json_config)
        # set the peers appropriately!
        the_json["Peers"] = peerlist

        # dump back the json config
        the_json_config = json.dumps(the_json)
        # then renormalise to hjson
        new_conffile_contents = subprocess.run([
            "yggdrasil",
            "-normaliseconf",
            "-useconf"
        ], input=the_json_config.encode("utf8"), 
        capture_output=True).stdout

        with open(self.parsed_args.config_file, 'wt') as rewriting_config_file:
            rewriting_config_file.write(str(new_conffile_contents, encoding="utf8"))
        
                
    def run(self, mode: Optional[PROGRAM_MODE]=None):
        """
        Run the program in the given mode. If no mode specified, then we take from the args.
        """
        if mode is None:
            mode = self.parsed_args.program_mode
        
        if mode == PROGRAM_MODE.LIST_REGIONS:
            self.mode_list_regions()

        elif mode == PROGRAM_MODE.LIST_SELECTED_PEERSET:
            self.mode_list_peerset(True)

        elif mode == PROGRAM_MODE.LIST_SELECTED_PEERSET_NO_LOCALISE:
            self.mode_list_peerset(False)

        elif mode == PROGRAM_MODE.MODIFY_CONFIG:
            self.mode_modify_config()


def print_uid_gid():
    print("euid: " + str(os.geteuid()) + " egid: " + str(os.getegid()))


def deescalator(sysargv_program_stripped, parsed_args=None):
    """
    Attempts to drop privileges.

    In particular it checks to see if we are a worker and if so goes to that,
    else it attempts to fork with a new set of arguments.

    This expects arguments with the program stripped from the front.
    """
    # This allows help with ./<progname>
    if len(sysargv_program_stripped) == 0:
        sysargv_program_stripped = sysargv_program_stripped[:] + ['-h']

    parsed_args = parse_args(sysargv_program_stripped) if parsed_args is None else parsed_args
    # This is the plain one...
    is_worker = ("work" in parsed_args and parsed_args.work)
    if is_worker:
        # Pick the last config file including overrides
        parsed_args.config_file = ([parsed_args.config_file] + parsed_args.config_override_array)[-1]
        # If we have been told to drop privs, do so
        if parsed_args.drop_uid is not None and parsed_args.drop_gid is not None:
            # We have to use euid/egid, because uid and gid - if called with root -
            # set ALL processes to the provided uid (not root)
            # seteuid changes the local process rather than also the calling process 
            # in the case of root. We set ourselves with euid. 
            # https://linux.die.net/man/2/setuid
            os.setegid(parsed_args.drop_uid)
            os.seteuid(parsed_args.drop_gid)

        with Context(None, parsed_args) as ctx:
            ctx.run()

    else:
        # Attempt to deescalate....
        # also install first if necessary ^.^
        with YggdrasilInstalledContext(
                parsed_args.auto_install_yggdrasil and needs_yggdrasil_installed(parsed_args.program_mode), 
                parsed_args.packager_type_override
            ), tempfile.TemporaryDirectory() as our_directory:
            # See if we are sudo :)
            sudo_uid = os.getenv("SUDO_UID")  # these are strings but it's ok because 
            sudo_gid = os.getenv("SUDO_GID")  # they will be passed as arguments.

            if sudo_uid is not None:
                sudo_uid = int(sudo_uid)
            if sudo_gid is not None:
                sudo_gid = int(sudo_gid)

            with PriviligedContext(
                    parsed_args.program_mode, 
                    our_directory, 
                    (sudo_uid, sudo_gid),
                    parsed_args
                ) as priv_ctx:
                # check if we are in a sudo environment and have someone to deelevate privileges to.

                new_arguments = (["--work"] + 
                    sysargv_program_stripped + 
                    priv_ctx.get_worker_additional_args()
                )

                # Now, we fork!
                our_pid = os.fork()
                if our_pid == 0:  # Child process ^.^
                    deescalator(new_arguments)
                    # This bypasses normal exit processing, which prevents
                    # calling the __exit__s in the (shared because fork works that way) 
                    # "with" contexts and breaking the directory removal in the 
                    # parent process.
                    os._exit(0)
                else:
                    # simply wait for the child pid to finish
                    pid, exit_status = os.waitpid(our_pid, 0)

            

def main():
    argvstripped = sys.argv[1:]
    deescalator(argvstripped)

if __name__ == "__main__":
    main()
